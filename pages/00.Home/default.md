---
title: Courseware
---

This site is a collection of my teaching materials, here you can find the work I have developed for the year-long full-time courses that I have developed, as well as workshops and other shorter activities. All work is released under a creative commons attribution license.

Please understand this repository as a living document, it is changing all the time. Every class that I give I always make a slight tweak or add a whole new section or remove something that is no longer relevant. So expect broken links and ocassional typos in new material. I do my best in trying to keep it tidy.

In collecting these materials I have used many online resources from which I borrowed, on occassion, a particularly good explanation, an image or an inspiring idea for a sketch. The acknowledgement section of every page tries to gather credit for all these sources. Please let me know if you find omission.

If you are in either of these three courses you can start with these links:

- [Master Digital Design](/master%20digital%20design)
- [IDA2 - Critical Engineering](/data/second-year)
- [IDA3 - Critical Engineering](/data/third-year)
- [Materials for 2020 workshops](/workshops/the-otherness-toolkit)
