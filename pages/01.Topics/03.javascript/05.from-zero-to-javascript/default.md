---
title: From Zero to Javascript
---

Let's go through the basics of how to code in Javascript. Open the Mozilla [Firefox browser](https://www.mozilla.org/en-US/firefox/all/#product-desktop-release), open the `developer tools` and select the `console` tab. Alternatively you can press **cmd+alt+k**.

#### Let's type along

And misuse Javascript as our calculator:

```js
3 + 3
567775 - 5
3 * 3
3 ** 2
16 / 4
32 / 6.5
((32 * 2) / 4)
64 > 50
50 < 70
50 < 50
50 <= 50
50 >= 50
50 == 50 // compare
```

Let's look at the `++`, `--`, `=-` and `=+` operators, they are strange but they will be handy pretty soon.

```js
v = 5
v++
v++
v++
v++
v--
v--
v += 3
v += 2
v -= 4
```


```js
console.log("Most trees have more than 4 leaves, except dead trees");
```

```js
document.write("Most trees have more than 4 leaves, except dead trees");
```

```js
document.write("<h1>Most trees have more than 4 leaves, except dead trees</h1>");
document.write("<h4>Dead trees do not have leaves</h4>");
```

- What is going on here? 
- What effect do these statements have? 
- What is different about the way that the message gets printed? 
- What happens if we do not type that dot? 
- What happens if we do not type that semicolon at the end? 
- What happens if we put spaces around the dots?

Each of these lines is a **statement** and you can think of a **statement** as an action of some kind: 
- **jump**
- **pick one red ball**
- **drink one sip of water** 
- **turn left**
- **walk ahead 10 meters**
- **swap sweaters with the person next to you**

That's what a statement does, it instructs your program to do something, anything. Whatever gets you closer to your ultimate goal.

#### Storing things

Imagine that we are told that in our frontend we have to stay flexible, that our researcher just found about some trees that do not have leaves but they are also not dead, so we might want to show something else there instead of *dead trees*. We can use a **variable** to store our special, exceptional tree and later decide what to show.

```js
except = "dead trees";
console.log("Most trees have more than 4 leaves, except " + except);
```

**Variables are used to temporarily store things in our programs**.

Variables normally have a name, a datatype (the kind of thing that they are storing) and a value.

```js
<name> = <value>

or 

[var|let] <name> = <value>
```

In javascript you will also see variables sometimes preceded by a keyword like `let` or `var`, these keywords will become important later, but at this stage all we need to understand is that **they are used to define a variable**, so whatever follows `var` or `let` is the name of a variable and whatever follows the `=` sign will be the value of that variable.

The value of our variable might change during the execution of our script.
In the example above our value has the value `"dead trees"` and is of a data type called **string**. A **string** is an alphanumeric colletion of characters and it is often used to represent text. But variables can have other datatypes too, they can be fdifferent kinds of numbers, they can be lists of things and if there is no datatype that satisfies your needs, you can always make your own datatypes.

To learn more about Javascript's basic data types, [follow this link](https://javascript.info/types).

Let's go back to our example. Our researcher just told us that "pine trees do not have leaves, they have needles instead". Oh! that's a nice exception let's go back to our script.

```js
var except = "pine trees, they have needles instead";
let howmany = 4;
console.log("Most trees have more than "+ howmany +" leaves, except " + except);
```

- What changed from the previous example?
- Let's mess around with the syntax, what happens if we miss the semicolons here and there?
- Why do we have two variables and how are they used?
- What is the difference in how one variable is used compared to the other?
- What happens if we do not give our variable a value?

#### Time always moves forward

A computer program like the script above, runs in a certain amount of time, the computer executes one instruction at a time in an ordered sequence, so the first instruction goes first, the second goes after that and so on. 

The idea of an **ordered sequence** is important when we are scripting. How the **arrow of time** moves within our program as the execution progresses is called **flow**. If we let things be and our script has a straight **flow**, things will simply be executed in a linear sequence until our program has ran out of statements, no surprises there. 

But if we alter that flow by using a special kind of statement called a **flow control statement** we can jump around our program and alter the sequence of statements that gets executed. So maybe we can skip some statements under certain conditions and execute a different sequence when it's Tuesday for example.

#### Flow control

##### Block

A **block** of statements in Javascript is declared by surrounding it with curly brackets. You can think of a **block** as a group of statements that belong together and are executed one after the other. For example the instructions to do a laundry in a washing machine could be defined as a block.

```js
{
  // open door
  // load our dirty laundry
  // close door
  // open detergent drawer
  // pour detergent in the correct drawer
  // pour softener in the correct drawer
  // close drawer
  // set correct program in program wheel
  // start washing cycle
}
```

Block statements are often used together with other flow control statements (e.g. `if`, `for`, `while`)

##### Conditional expression

A conditional expressions can be tested to be `true` or `false`, a kind of *yes* or *no* question, like for example: is an apple a fruit? is the person's height greater than 170cm?

Conditional expression are normally used as conditions in all flow control statements. They are the way in which you can test conditions in your code to control execution flow.

Examples of conditional expressions:

```js
"apple" == "orange"
"apple" != "orange"
65 > 20
30 == 30
"apple" == "apple"
```

Conditional expressions can be compounded or chained together, to conflate various comparisons into a single condition. This is useful for example when you want to ask a question that has various conditions that need to be met for the answer to be `yes` or `no`, such as for example: is this person from London `AND` above 18 years of age? Is Northern Ireland part of the United Kingdom `AND` part in the Island of Ireland? Is mozarella a soft cheese `AND` of Italian origin? The `AND` in this case is expressed by using the double-ampersand `&&` symbol.

```js
(age > 18) && (city == "London")
```

It is also possible to test two conditions and get a positive result if only one of them is true. In this case we use the `OR` operator, denoted by a `||` symbol. Is a person blonde `OR` ginger?

```js
(hair == 'blond') || (hair == 'ginger')
```

In this case the whole conditional expression will be true, if only one of the conditions is true.

##### Javascript quirk: Falsyness

The following values evaluate to false (also known as Falsy values), so any variable or conditional statement inside of an `if` that evaluates as **falsy** will result in the `if` condition not being met:

- `false`
- `undefined`
- `null`
- `0`
- `NaN`
- the empty string ("")

So this means that the code inside of this `if` statement will never be executed:

```js
if("") {
    // will never execute what's inside here
}
```

The **condition** in the `if` statement is normally expressed as a conditional expression, using the [comparison operators in javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Comparison_Operators), follow link for details.

##### If statement

The `if` statement is used to test a condition, if that condition is met it will execute one block of code (containing statement A) and if the condition is not met it will execute the block of code after the `else` (containing statement B). If no `else` clause is indicated and the condition in the `if` isn't met, the flow will continue with the first statement after the `if` block.

```js
if (<condition>) {
    // statement A
} else {
    // statement B
}

// statement C (outside the if..else)
```

There are other fancier ways of doing more sohisticated condition checking and flow control that are sometimes more approrpiate to use than the `if` statement. [Read more about them here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling). For this initiation session we will have enough with `if` only. 


###### Looping and iterating: For statement

The word *iteration* is a fancy computer-sciency word that is used to mean *repeating something a specific number of times*. For example, in plain english one could say something like "repeat my name until sunrise", this would be a kind of iteration. We have an action expressed by a statement like: "repeat my name" and a condition to terminate the iteration expressed by the condition: "until sunrise".


```js
for (<initial state> ; <loop condition> ; <step>) {
    // statement
}
```

The syntax is a bit tricky, why would you need three expressions in that parenthesis!? Well that has to do with the fact that a `for` statement normally needs a counter to keep track of how many times it has done something. A kind of index so that it can keep asking itself *"how many times have I done this!?"*. Let's say that we have a tree with 20 leaves and we want to drop them all one by one. We could use a for statement such as this:

```js
howmanyleaves = 20
for(current = 0; current < howmanyleaves; current++) {
    console.log("dropping leaf: " + current);
}
```

As you can see the variable with the name `current` is used as an internal counter to keep track of how many times the loop has run.

The `for` statement is not the only way to create a loop the are other ways that might be more appropriate given your usecase. But for this beginning sessionwe will only cover `for`, but you should also learn about `for..in`, `while`, `break` and `continue` in your own time as you will soon come across use cases where `for` will not be the best things to use. [Continue reading about loops here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration).

[Watch this video of the CodingTrain for a more detailed explanation and some visual examples](https://www.youtube.com/watch?v=cnRD9o6odjk).

#### Wrap up

That's it! That's all you need to get started scripting. Of course this is only a good start and there is quite some things that I have intentionally left out, but with these basics you will be able to do pretty sophisticated stuff fairly soon and the rest will come to you as you need it.

Javascript is a very versatile language with some quirks of its own, it will take a while before you become proficient in the entire language. Be patient and practice! Get yourself a good cheatsheet (choose from the suggestions below), print it out and have it near your computer in your first weeks.

There are many online cheat sheets and quick references that will be handy to keep by your side as you do your first try outs.

- [Full 13-page cheatsheet(s)](https://ilovecoding.org/blog/js-cheatsheet)
- [https://htmlcheatsheet.com/js/](https://htmlcheatsheet.com/js/)
- [Modern JS cheatsheet](https://github.com/mbeaudru/modern-js-cheatsheet) 

There are other notions important to programming that we will encounter as we start writing slightly longer pieces, such as functions and objects, but you will be introduced to those on a need to know basis as you progress.

#### Keep exercising your javascript-fu
- [30 days, 30 exercises](https://javascript30.com/)

#### Deeper into the rabbit hole

The [How to Read Software](/topics/how-to-read-software) chapter goes a little deeper into the idea of programming languages as an interface and contains a few references that hopefully will inspire you to think of code as a kind of UI.
