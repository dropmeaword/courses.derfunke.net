---
title: Enough Javascript to make shiny things
---

The following sessions cover Javascript in a super-speedy approach. The idea behind this approach is not so much to get you into a Computer Science way of understanding the language but rather as a quick sketching medium for dynamic webthings.

Our canvas will mostly be the browser, so some previous knowledge of HTML/CSS helps. In the *rabit hole* section of every section you will find materials to further your knowledge, these are more advanced topics, tools and handy links that will hopefully support your learning further.

- We start with [From Zero to Javascript](/topics/javascript/from-zero-to-javascript) by opening a browser and dropping to its lowest level, the Javascript REPL console.
- As your projects grow you will need to manage your assets. In [Going Gold](/topics/javascript/going-gold) you will find some tips for tools to make a more professional workflow.

