---
title: Delightful and critical
---

It seems that everywhere you look every story related to technology since 2013 is a dystopian hell scape where there are tech giants that play the role of the great victimizers and the rest of us whose data is carelessly extracted and are permanently vulnerable to abuse. The narrative around technology has shifted significantly as the practices of the industry and the scale of its influence has changed too. So much of what we perceive as innovation or our relationship with technology has so much to do with the stories we tell ourselves. I wanted to share with you three works of masterful storytelling that in my opinion succeed at reaching a rare sweet spot of being critical while remaining delightful. So much of the critical discourse around technology is brainy, based on evidence and argumentative that it can also be exhausting to follow, causing fatigue and anxiety. The three works that I wanted to screen today are by two authors and they tell the story of the human, of how the human feels in tha face of innovation, of our vulnerabilities and anxieties, our wishes and projections.

### Mike Mills

The first work is by Mike Mills and is called **Future Interviews**, [can be seen in full in this Vimeo link](https://vimeo.com/79277863).

> Mike Mills interviewed about a dozen children of people who work at Silicon Valley. Some of these parents are engineers, others work at a Google cafeteria.

> Aged eight to twelve, these children are the ones who will actually be inhabiting the future that Silicon Valley is busy creating.

> First, the filmmaker asks them to speak briefly about themselves. That’s as charming as you might expect. Soon, however, Mills casts the children as futurologists. It turns out that when they are asked about what tomorrow will bring, kids can be quite ambivalent. Some look at the future with bright, enthusiastic eyes. Others are disillusioned. Most present a mix of enthusiasm and pessimism. On the one hand, they embrace technology. They play Minecraft and think that because we have access to so much knowledge, we must be smarter than our ancestors. But their vision of tomorrow can be bleak too: we won’t have trees anymore, we’ll get holographic plants instead; people won’t meet each other physically because the machines will mediate all human interactions, there probably won’t be any animal around either.

Quoted text by Regine Debatty [source](https://we-make-money-not-art.com/strp-a-festival-thats-not-afraid-of-the-future/) (accessed 06.01.2019) (slight edit 06.09.2019)

##### To do while you watch
Keep an eye for dystopian and utopian narratives
- what characterizes a utopian or dystopian thought in their narratives?
- how do their utopian narratives have in common? or how do they differ?

### Miranda July

#### Somebodyapp

The second work is a relatively old campaign that Miranda July did for Miu Miu. It is called Sombeodyapp, this was actually released an an app and in 2015 (last time I checked) it was still available in the app stores. The short film that is used to *advertise* the app however is where we can see this delightful quality that I would like to consider.

<iframe width="560" height="315" src="https://www.youtube.com/embed/iz13HMsvb6o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


#### A Handy Tip for the Easily Distracted

The last work is a clip from Miranda July's 2011 film **The Future**.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Yc57X0j_UwM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

