---
marp: true
title: V/A/M/XR and the body as the realm of synthetic experience
theme: gaia
size: 16:9
paginate: true
---

### Session plan

- 10.00 Recap on CO2 emissions session
- 10.30 Lecture on VR the body and synthetic experience
- 12.00 open discussion
- 12.30 (( break ))
- 13.30 visit to VR

---

## V/A/M/XR and the body as the realm of synthetic experience
![nasavr](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1583224897/VRARMRXR/NASA-VIEW-HMD-2.jpg)

---

## VR a personal story
Full of disappointment

---

## The 90s Imaginaries

<iframe width="560" height="315" src="https://www.youtube.com/embed/zzwPuJklv4w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

To a digital *metaverse*, where even childhood memories can be hacked with VR. Where the good guys and bad guys, fight it out in a world made of data.

<iframe width="560" height="315" src="https://www.youtube.com/embed/UzRjtvMQds4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Keanu Reeves = FUTURE

---

VR has been around the corner as the promised future since the 80s at least, and humans have projected their desires and fears on VR as the ultimate tech, the one that will liberate us from physical reality and bring us to a new epoch of unlimitted potential. At least that was the promise.

---

## VR in the 90s

<iframe width="560" height="315" src="https://www.youtube.com/embed/ACeoMNux_AU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

Since then it has been a specialized medium that required of very expensive equipment that was only accessible to research centers, universities and the military or very specific showcases.

---

## Until 2012

![rift](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1583224897/VRARMRXR/oculus-rift-original.jpg)

When Palmer Luckey launched a Kickstarter to bring the Rift to market.

---

Between 2012 and 2017 VR tech has been maturing steadily and research on VR has exploded. Google, Facebook, HTC, Samsung, Sony, Microsoft, Leap Motion all have jumped on the promise a VR as a mass-consumer medium that is just around the corner... 

---

## New imaginaries (circa 2016)

<iframe width="560" height="315" src="https://www.youtube.com/embed/2MqGrF6JaOM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Microsoft's futuring has always been a bit... meh. (maybe it needs some Keanu Reeves in there).

---

This is a Magic Leap demo from around the same time. (2017)

<iframe width="560" height="315" src="https://www.youtube.com/embed/GmdXJy_IdNw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

Yet for all its promises and incredible progress, VR doesn't quite seem to catch on beyond a niche in some industries (and gaming which is a very gadget-friendly industry to begin with).

---

### VR, AR, MR and XR a glossary

![vrmrar](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1583224897/VRARMRXR/vr_mr_ar.jpg)

---

In VR you are immersed in a completely virtual world while the physical world around you is blocked from view by the screens in front of your eyes.

---

In AR a digital overlay is placed on top of a video feed overlapping it. Virtual object from the digital layer do not directly interact visually with the camera feed. It works as a kind of real-time green screen.

---

In MR there is no video feed, but the visor or headset is see-through and allows you to see the physical world around you, while at the same time it is scanned directly into a digital model of the physical world, where the digital artifacts can interact with the physical world, doing things such as occlusion, etc.

---

See engineers from Magic Leap show this interaction between physical and digital:
<iframe width="560" height="315" src="https://www.youtube.com/embed/MKFnBfxOZPo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

The term **XR** is used to refer to all three of the above indistinctively. Typically, people that develop content for one of these technologies are familiar with techniques used in the others, so it's not like they are spearate skills.

---

To add to the confusion, it seems that there are uses of AR that do not involve the scanning techniques used in MR but that behave and look like AR. See here:

<iframe width="560" height="315" src="https://www.youtube.com/embed/0Y13DxRANO0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Prediction #1: In the future your Pokemons will be much harder to catch.

---

**Prediction #2: this mess of temiinologies and nuanced distinctions, will very likely converge in the not too distant future.**

So hold them lightly.

---

XR is not just one technology, but a constantly evolving assembly of different technologies that facilitate immersive experiences. It helps to understand the past, present and future of XR to know a bit about these technologies.

---

## Spatial Computing
Is a modality of computing in which the interface is aware of its surroudings and takes that into account in the process of presenting information to the user. As a general rule, **any time you need to know the location, size, shape or orientation of something, you are using spatial computing**.

---

#### Skymaps 2009
<iframe width="560" height="315" src="https://www.youtube.com/embed/p6znyx0gjb4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

#### Niantic Labs 2013 (Google spin-off)

<iframe width="560" height="315" src="https://www.youtube.com/embed/92rYjlxqypM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Niantic took the ideas behind *spatial computing* and combined them with Google's mapping products and advertising channels and turn them into a viable gaming platform

---

#### Pokemon GO (2016)

This is when Snorlax appeared in Beitu district in Taipei.
<iframe width="560" height="315" src="https://www.youtube.com/embed/p_0h4_5q_5o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Spatial computing* fever pitch.

---

### Stereo Vision

![stereoviewer2](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1583224897/VRARMRXR/stereo-photo-viewer.jpg)

A "perceptual technology" that relies on how we perceive visual stimuli.

---

Each eye perceives a different perspective of reality and the brain merges them together and constructs a 3rd dimension that relates to depth.

![stereoviewer](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1583224896/VRARMRXR/CLSM-stereovision.jpg)

Depth is a mental re-construction.

---

![stereo](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1583224896/VRARMRXR/25022020_vr_slides00003.png)

---

### An Intermezzo
To give recognition to a genious idea.

---

### The Case Google Cardboard
A most significant and elegant hack that helped boost the current wave of VR.

![cardboard](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1583224897/VRARMRXR/gif-cardboard-2.gif)

Prediction #3: Cardboard will continue to be unaatainable goalpost for **portability** in VR systems for years to come.

---

### Cardboard-like systems
This includes most of Google's VR and Samsung VR offerings:
- Passable immersion
- Cartoonish VR
- Half-decent 360 video player
- Unconvincing
- Toy
- Best experienced while sitting

---

![crueltytograndma](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1583224897/VRARMRXR/vr-grandma-meme.gif)

---

### Head tracking
It is what allows the subjective view point that characterizes these experiences. Without head tracking, there's no VR.

![htracking](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1583224897/VRARMRXR/head-tracking.jpg)

In early VR systems head tracking was a very expensive thing to do.

---

### 360 video
The appearance of cheap VR headsets also pushed 360 video camcorders down in price and size. Many content creators considered these two technologies to go hand in hand.

Night Fall - Dutch National Ballet
<iframe width="560" height="315" src="https://www.youtube.com/embed/7VhAUKThR1Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Audio spatialization
Crucial, yet often overlooked by designers.

<iframe width="560" height="315" src="https://www.youtube.com/embed/34Y0dwVBq4c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Hand tracking
<iframe width="560" height="315" src="https://www.youtube.com/embed/s390u_qXwo0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

The original HTC Vive couldn't do finger tracking, the hand presence was comparable to a rigid stick, in fact the controller is called wand.

<iframe width="560" height="315" src="https://www.youtube.com/embed/TckqNdrdbgk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Hand-object boundary

This is the first of our "unresolved" problems in VR today.

![handobjectbounds](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1583225279/VRARMRXR/dimple-mesh2.gif)

Prediction #4: You will see a lot more of this in the coming months.

Visual ASMR.

---

### Finger tracking

Without proper finger tracking all you had in VR were *Barbie Hands*.

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/cjXSXmHZP3Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

Recent work by [@pushmatrix](https://twitter.com/pushmatrix) (Daniel Beauchamp).
<iframe width="560" height="315" src="https://www.youtube.com/embed/HfgCS_tTcPs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

#### Haptic feedback

We use the word haptic when we mean to say that something can be sensed through touch, normally through our skin. 

The most common kind of haptic feedback used in VR is vibrohaptics.

---

### Gamer VR vibrohaptics
Full-body suits for VR: [B Haptics](https://www.bhaptics.com/tactsuit/) or [KOR-FX vests](http://www.korfx.com/).

Fetish.

---

#### Machine Vision
![sensors2](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1583224896/VRARMRXR/HTCsensor.jpg)

---

#### Machine Vision
![sensors1](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1583224897/VRARMRXR/vr-sensors.png)

---

Theses sensors make room-scale VR not easily portable among spaces.

This is another one of the VR problems that have made VR too specialized a medium so far.

---

The latest generation of headsets (Oculus Quest and Vive Pro) have these sensors built into the headset, which theoretically removes the need to have a dedicated room and makes the system more portable and removes the need for room calibration.

---

## (( break ))

---

### Foveal tracking

FOVE headset
<iframe width="560" height="315" src="https://www.youtube.com/embed/LNtu5sbrzEA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### AR Light Estimation

<iframe width="560" height="315" src="https://www.youtube.com/embed/7Kk6iVr5ULo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Some uses of xR related tech

---

## Shopping-related

IKEA AR Place app
<iframe width="560" height="315" src="https://www.youtube.com/embed/Arbeh7vSIi8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

IKEA AR Place app corporate promo
<iframe width="560" height="315" src="https://www.youtube.com/embed/UudV1VdFtuQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

IKEA PLACE AR app easter egg
<iframe width="560" height="315" src="https://www.youtube.com/embed/FEaFxORXkhw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Other bodies

Ali Eslami SnowVR, album release with Ash Koohsha.
<iframe width="560" height="315" src="https://www.youtube.com/embed/k4wHILDQljU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### The Machine to be Another

<iframe width="560" height="315" src="https://www.youtube.com/embed/_Wk489deqAQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### "VR for Good"

VR has been touted as the ultimate *empathy machine*.

---

Zikr from film maker [Gabo Arora](https://lightshed.io/) using 360 video and Depthkit.
<iframe src="https://player.vimeo.com/video/351082249" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/351082249">Zikr A Sufi Revival - An Interactive Social VR Experience - Trailer</a> from <a href="https://vimeo.com/barrypousman">Barry Pousman</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

---

"4 Feet: Blind Date" is an award-winning 360 film.

<iframe width="560" height="315" src="https://www.youtube.com/embed/5SiV0w6SRFk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/ERffRXjTAqM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Military training
<iframe width="560" height="315" src="https://www.youtube.com/embed/NND7Hk5fYdI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### VR venue experiences
Perhaps the most famous in this kind of experiences is The VOID.
<iframe width="560" height="315" src="https://www.youtube.com/embed/cML814JD09g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/WUs5qi_RFnM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## VR as a place that you visit

Milam Wisp - HOUR
<iframe width="560" height="315" src="https://www.youtube.com/embed/h06f4WacoZk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

Ali Eslami - False Mirror
<iframe width="560" height="315" src="https://www.youtube.com/embed/h-mc8bjendE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## ARKit artworks

<iframe width="560" height="315" src="https://www.youtube.com/embed/Z3lY79ZLMsI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

#### Zach Lieberman
<iframe width="560" height="315" src="https://www.youtube.com/embed/ET2CKUqdPCo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

#### Weird Type by Zach Lieberman:
<iframe width="560" height="315" src="https://www.youtube.com/embed/UzNCGqE9MhE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

#### ARCore
Mapbox navigation
<iframe width="560" height="315" src="https://www.youtube.com/embed/N6YwOHNej1Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

#### Social VR (Shared VR)
- [AltVR](https://account.altvr.com/worlds/featured)
- [Facebooks Horizon platform](https://www.oculus.com/facebookhorizon/)
#### Authoring
- [Facebook Quill](https://quill.fb.com/)

---

# Concerns in using VR as an interactive system

---

**Motion sickness**: Performance is crucial in VR, if the work performs badly it can introduce motion sickness in the user which in some cases can lead to vertigo, loss of balance, confusion, etc.

---

**Situational awareness** Room-scale VR blocks the user's perception of the physical space they are in. (switch to MM slides)

---

**"The Rat's Tail"**

![ratstail](https://res.cloudinary.com/zilogtastic/image/upload/c_scale,w_1100/v1583224897/VRARMRXR/rats-tail-management.jpg)

---

**Infinite Walking** (saccadic redirection)

---

**Time Dilation** 

---

**Depth perception** having a screen 6cm from your cornea displaying heavily distorted graphics is not how reality works. After being for a while in VR some users experience several minutes of re-adaptation to RL conditions.

---

**Consent in VR** (abuse has been recorded in most social VR experiences)

---

**Transition from VR to RL** (e.g. not a good idea to drive right after VR)

---

**Zeitgebers** VR affects circadian rhythms, our perceptual systems evolved over thousands of years to take cues from nature to tell us what time of day it is. These can not be easily simulated.

---

# Body & Perception

---

##### Ehrsson lab experiments
- [The rubber hand experiment by the Ehrsson lab](https://www.youtube.com/watch?v=DphlhmtGRqI)
- [Work by the Ehrsson Brain, Body and Self Institute](http://www.ehrssonlab.se/)
- [Barbie-doll experiment by the Ehrsson lab](https://www.youtube.com/watch?v=uhRbAjdEiGw)
- [Out of body experience](https://www.youtube.com/watch?v=ee4-grU_6vs)
- [The Illusion of having three arms](https://www.youtube.com/watch?v=GZDDWozq3b4)

---

Visual stimuli can be effective conveyors of experience but only is accompanied by experience in other cognitive modes.

---

# ((brief break))

---

# VR as a synthetic experience

(switch over to MM slides)

---

<!-- _backgroundColor: white -->

---

# Let's meet again at 13h30
