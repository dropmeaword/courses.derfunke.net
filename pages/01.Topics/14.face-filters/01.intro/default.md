---
title: Introduction
---

Face filters are perhaps the most widely used application of AR thanks to them becoming integrated in the apps we use to interact in social media. While face-based augmentation techniques exist since way before social media picked up on them ([see face swap from the openFrameworks community](https://vimeo.com/29279198)), companies like SnapChat started incorporating some of these techniques into their tools, eventually Facebook caught up to this in Instagram and face-filters are now one more type of content that people can post in social media easily.

Face filters are definitely a strange technology, they rely on various other technologies such as computer vision and machine learning and combine several very sophisticated algorithms in real-time to achieve their effect. Furthermore the tools for the production of this type of content are using paradigms often seen in more sophisticated game engines, so they are a great gateway to put into practice more sophisticated principles used in computer graphics in other tools.

In this module we will become a bit more familiar with two tools, Lens Studio by Snapchat and Blender, the open source 3D creation suite. While Lens Studio is a fairly easy tool with lots of options, it is possible to become really good at it in just a few weeks. Blender however is a different beast, it can do all kinds of things, from 3D modelling to video editing, to realtime mocap, etc. We will only be using a small fraction of what Blender has to offer in this module, but I hope that this more practical approach gets you to be more familiar with the tool.

