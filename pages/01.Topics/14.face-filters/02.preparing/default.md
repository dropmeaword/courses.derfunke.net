---
title: Getting ready 
---

Unless you enjoy wrist pain, a mouse is strongly advised.

![3-button mouse](../media/3-Button.jpg?resize=400)

**Step 1. Download and install the software.** It is recommended that for all the work in this module you bring a 3-button mouse, and that you install both [Lens Studio](https://lensstudio.snapchat.com/) and [Blender 2.83](https://www.blender.org/download/releases/2-83/). There are newer versions of Blender, like 2.9 that were released in the last two months, but these tutorials are made for 2.83, so I advise you to download that one and start from there. Further down the line when you are more confident with the tool you can use whatever you like.

[Download Blender 2.83](https://www.blender.org/download/releases/2-83/)

**Step 2: Get a snapchat account.** You will also need a Snapchat account, you will need the phone's app for that purpose, so please install it and create the account. In the future we will not be using the phone's app so much, so if you want to delete it later on you will still be able to work on this course without the phone app installed. You only need it once to register.

**Step 3: Install SnapCam.** Please also download [Snap Camera](https://snapcamera.snapchat.com/) this is another product by SnapChat that allows you to use SnapChat Lens Filters as a virtual webcam. This will come in handy to test our filters and to create a graphics toolchain that we can use outside of social media for our own purposes.
