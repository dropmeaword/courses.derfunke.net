---
title: Session 2 - Enough Blender to make filters
---

In this session we will have a look at how to add 3D content into our filters and I will share a small technique I use a lot to create a library of "body parts" to compose my filters. We are going to be using Blender for this.

Now Blender is a huge friendly monster of a tool that can do all kinds of things, from 3D modelling (which is hat is known for) to video editing, motion graphics, games, etc. It has many functionalities and it takes a while to learn it. For our purpose we need only a few things and these few things can also serve as an introduction to Blender itself.

## Part 1 - the walkthrough
In this video we will see what each panel is, and the basics that you need to navigate Blender screens.

<iframe width="560" height="315" src="https://www.youtube.com/embed/_MRh9YkDvVg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Part 2 - sculpting with Blender
In this video we learn about the sculpting feature of Blender and how to create shapes for your filters.

<iframe width="560" height="315" src="https://www.youtube.com/embed/UG0uBKCCn1c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Part 3 - Importing 3D assets in Lens Studio

<iframe width="560" height="315" src="https://www.youtube.com/embed/ghZsJ2AK5PI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Part 4 - become a digital Frankenstein

The hermit crab used in this tutorial is in the assets pack that I prepped for you, [you can download it here](https://zilog.stackstorage.com/s/PNjnuwMuuX5vO725) 

<iframe width="560" height="315" src="https://www.youtube.com/embed/-Rf3dY98m_0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
