---
title: Session 4 - Matcaps and shaders
---

## Part 1: I'm sorry, matwhat!?

What is a matcap and why should you care?

<iframe width="560" height="315" src="https://www.youtube.com/embed/Bhf8gQZB7aE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Part 2: building a shader for matcaps in Lens Studio

Shaders are what we use to make our vectors look nicer. Here I take you through the steps of making a shader in Lens Studio, and actually the same technique will work in every other real-time engine out there.

<iframe width="560" height="315" src="https://www.youtube.com/embed/PABAl2gecgc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Even though the visual language to build shaders is something specific to Lens Studio, the math behind this specific shader will work in every other tool that you can build shaders with such as Blender, ShaderToy, Unity, Unreal Engine, TouchDesigner, and a very long etc.

## Part 3: bonus track

Because making things work fine is not always what you want. Messing around with numbers is why computers were invented in the first place, so please mess with your numbers.

<iframe width="560" height="315" src="https://www.youtube.com/embed/FlQCZSh1nyo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


