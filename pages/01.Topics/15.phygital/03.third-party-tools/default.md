---
title: Third party tools to design actions/reactions for our phygital device
---

# Teachable Machine

# P5js

# Framer

[Framer](https://www.framer.com/) is a prototyping tool that combines the best aspects of other prototyping tools, like Figma it does collaboration, like Protopie it has an easy to use workflow for animations, like Sketch it easily turns your vector drawings into visual components and unlike any of the above, is a tool designed for tight cooperation with dev teams, so a Framer prototype can be handed-off to a developer and they get fully functional React code that they can use directly in the implementation of the feature you are designing. Framer can also import your Figma and Sketch works so that you can use them as a basis to create higher quality prototypes.

To get rolling with Framer, the best place to start is [their own tutorials](https://framer.com/projects/tutorials). They also have really handy live walk-throughs of the tool, see [getting started with Framer](https://framer.com/projects/Getting-Started-in-Framer-copy--pj1FRGldOl2pkGI8eAgc-3IAKn?tutorial=-MDo4oTqFCE&node=HgMnKcaN3-page). 

#### Components in Framer

You might be familiar with the notion of component from other softwares like Figma or Sketch, in which a group of shapes and visual elements can be grouped together as a component. Framer works very similarly, but a component in Framer can also have behaviours, such how it responds and animates in certain circumstances and other user defined bahviours that you can write as Javascript.

Framer uses Reactjs under the hood, in fact you can think of Framer as a way of visually prototyping React applications.

## A sample starter project in Framer for IoT work

Sander prepared a small example for you to use as starting point in your Framer adventure. [You can check it out here](https://framer.com/projects/IoT-example--6opaW5mMYKJMxM4fGdeP-7VPiO).

#### Components in this Framer example

This example provides a couple of Javascript libraries that will talk to the MQTT broker and four basic components. The components are as follows:

- NumericReceiver, is a simple HTML header `<h1>` that renders a value received from the MQTT broker
- NumericSender, is an edit box that when you enter a value it will be broadcast to the MQTT broker
- Receiver, is a slider that renders a received numerical value
- Sender, is a slider that send out its current value as an MQTT message

## Sending the right messages to our phygital device

