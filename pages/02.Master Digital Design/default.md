---
title: Master Digital Design
taxonomy:
    category: mdd
---

Here you can find the course materials, notes and all associated resources used in my course at the Masters of Digital Design in the Amsterdam University of Applied Sciences.

If you come across any mistakes, please let me know.

Website of the program [Master Digital Design](http://www.masterdigitaldesign.com)

## Data + Matter 2019-2020
- Teacher: Luis Rodil-Fernandez
  - Office hours: Monday 13h00 to 17h00
  - Lab hours: Tuesday 10h30 to 17h00
  - Coaches on mondays by appointment only (book via Brightspace)
- Labs: Marcus Graf
- Assistant: Dolinde
- Syllabus [download link](media/2019-2020&#32;MDD&#32;-&#32;Data&#32;and&#32;Matter_&#32;making&#32;interactions&#32;-&#32;syllabus.pdf)

#### First semester

| Week 2 | | |
| :------------- | :------------- | :----- |
| **MON 09.09** | Introduction to the course and [Computational Thinking](/topics/computational-thinking) | Luis |
| **TUE 10.09** | [Mastering your canvas](/topics/browser-based-graphics/understand-your-canvas)  | Luis |
| **WED 11.09** | [From Zero to Javascript](/topics/from-zero-to-javascript) | Marcus |
| **FRI 13.09** | [Painting with code](/topics/browser-based-graphics/painting-with-code) + Basic input methods *(((could this be a lab perhaps?)))* |  |

| Week 3 | | |
| :------------- | :------------- | :----- |
| **MON 16.09** | [Mastering animation](/topics/browser-based-graphics/understand-animation) + short assignment | Luis |
| **THU 19.09** | Codelab (morning) |  |

| Week 4 | | |
| :------------- | :------------- | :----- |
| **MON 23.09** | [Prototyping like a pro](/topics/prototyping-like-a-pro) + with Artur Cordeiro + Philips case study | Luis |
| **TUE 24.09** | review assignment + [Learnable Programming](/topics/code-as-interface/learnable-programming) | Luis |
| **WED 25.09** | Working in the D+Mlab: introduction | Marcus |
| **TUE 26.09** | Codelab (morning) |  |

| Week 9 | | |
| :------------- | :------------- | :----- |
| **MON 28.10** | Introduction to [Interactive Systems](/topics/interactive-systems/gentle-introduction) + [Microcontrollers](https://gitpitch.com/dropmeaword/arduino-primer?grs=gitlab) | Luis |
| **TUE 29.10** | [Arduino Primer](https://gitpitch.com/dropmeaword/arduino-primer?grs=gitlab#/4) + [Little Sunrise Farm](/topics/interactive-systems/exercise-arduino-unity) + [Sensing the world around you](https://gitpitch.com/IDArnhem/sensing-the-world#/) + [Capacitive sensing](https://gitpitch.com/IDArnhem/capacitive-sensing#/) | Luis |
| **WED 30.10** | Getting things to talk to each other | Marcus |
| **THU 31.10** | Smartphone as a tool to sketch quickly + Unit project briefing | Luis & Marcus |

| Week 11 | | |
| :------------- | :------------- | :----- |
| **TUE 12.11** | Codelab (morning) |  |

| Week 12 | | |
| :------------- | :------------- | :----- |
| **TUE 19.11** | Codelab (morning) |  |

| Week 13 | | |
| :------------- | :------------- | :----- |
| **TUE 26.11** | Codelab (morning) |  |

| Week 14 | | |
| :------------- | :------------- | :----- |
| **TUE 03.12** | Codelab (morning) |  |

#### Second semester

## Syllabus
