---
title: Digital Time
taxonomy:
    category: 
        - artez
        - Y3
        - S1
---

Make a clock.

Your clock must avoid using the conceptions of time that we typically use, such as seconds, minutes and hours.

Your clock should place focus on the present.

Read the PDF hand-out “The Space Between the Ticks” from Present Shock, 2004, Douglas Rushkoff that was sent to you. What does the author mean by Chronos and Kairos?

### References
- Look at Ted Hunt's [Sense of Time](http://senseoftime.info/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/ndVhgq1yHdA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZQHubIMP-as" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

