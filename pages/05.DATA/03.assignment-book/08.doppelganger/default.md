---
title: Browser History Doppelgänger
taxonomy:
    category: 
        - artez
        - Y2
        - S1
        - 2015
---

### Part 1
Choose, two people from your family or group of friends and ask them to take part in your research by giving you access to their browser histories for a whole week.

Export these browser histories to CSV format, so that they are easier to work with. Try to get at least the URL visited, the date and time of the visit, but the more info you get the better.

Give each one of these browser histories an invented name (not the actual names of their owners) that will help us identify each of them. Exchange these browser histories with your classmates so that each has two people that they know nothing about.

Go through each of these browser histories carefully and try to form a picture of the person that visited all those sites.

Who are they? What are they looking for? What are they interested on? What's their age? Do they have children? Pets maybe? Are they planning a holiday? What do they use the web for in their mornings? And in the evenings? Did they buy anything in those three weeks? How much was it? What newspapers do they read? What music do they listen to? How old are they? What's their gender? And their favourite colour?

What can we say about this person based on observing their browsing habits? Write a paragraph that gives an impression of the person from what you have learned from their browser history. You can speculate but try to base your impression on actual clues your found in their browser history.

e.g. John: you are an early person that likes to check your social feeds in the morning and you unplug from the net early before bedtime. You are interested in music and fashion. You are around 24 and you are more inclined towards left-wing ideas. You are a comparative shopper that likes to look for the best deal before you buy anything. You like black shoes. You went to a DeerHoof concert last week. You have a dog and your house has a garden.


#### How can I get a browser’s history and export it?

- For Windows browsers: [Browsing History View](http://www.nirsoft.net/utils/browsing_history_view.html)
- For Chrome (on any platform)
[you can follow these instructions I wrote](https://github.com/christiangenco/chrome-export-history)
or [install this browser extension](https://chrome.google.com/webstore/detail/history-trends-unlimited/pnmchffiealhkdloeffcdnbgdnedheme)

### Part 2
Design two imaginary ads targeted to the Doppelgängers in the previous section of this assignment, one for each person. 

The ad must fit in one of the IBA Ad Unit formats, choose whichever one you fancy,

![IBA](iab-bme-branding-display20120511-12-728.jpg)

You can use any tool you like, but have a look at [Google Web Designer](https://www.google.com/webdesigner/), it has a ton of templates.
