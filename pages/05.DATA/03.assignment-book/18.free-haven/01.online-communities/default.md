---
title: Online communities
---

### Discussion

Students were given Chapter #9 of Claire L. Evans book *BROAD BAND* last week.

- What are your first impressions from the text?
- What is that chapter about?
- What communities does she talk about?
- What prompted Stacy Horn to start ECHO?
- What ideas did you find interesting in this text?

This is Stacy Horn, at the time of this interview in 1992 the web didn't exist yet.

<iframe width="560" height="315" src="https://www.youtube.com/embed/lDQ2o_Prgas" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/lV7TjxTl_XM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Communities

![](media/../../02.demoscene/media/xkcd-community.png)

#### ECHO

![](/topics/social-computing/ECHO-stacy_horn_1995.jpg)
![](/topics/social-computing/ECHO-ad-voices-in-your-head.jpg?height=600)

#### Before ECHO
The chapter you read makes mention of something called Project One and something called Community Memory. Let me tell you a bit more about that.

Here's a bit more on the history of [Project One](http://www.foundsf.org/index.php?title=Project_One_Warehouse) and [this](http://www.foundsf.org/index.php?title=Project_One:_Pueblo_in_the_City).

Project One was home to **"Resource One"** the first civic computing project, it was running in an old military surplus computers that some of the people from Project One got their hands on. **Resource One**, was a listing of NGOs working on social issues in the San Francisco area, a way for these organizations to know one another and exchange knowledge and resources. It was maintained by hand by Pam Hardt.

#### Community Memory

Another initiative that came out of the Project One community was Community Memory, which was the original BBS.

![](/topics/social-computing/media/community-memory-record-store-machine.jpg?width=700)
![](/topics/social-computing/media/CM_Booth.jpg?width=700)
![](/topics/social-computing/media/ACOMMUNITY.jpg?width=700)
![](/topics/social-computing/media/community-memory-cm-group.jpg?width=700)
![](/topics/social-computing/media/Community-Memory-printouts_20170207_180031.jpg?width=700)
![](/topics/social-computing/media/1972-10-PCC-cover.jpg?width=700)

The first version of it was installed in a record store, a local hangout where some of the people from *Project One* used to go to listen to records. It was conceived as a way to post short messages, exchange ads, etc. A bit like what you still see in many Albert Heijn suppermarkets in The Netherlands.

![](media/ahmessage-en.png)
![](media/ahmessage-small1.jpg)

## Modems and acoustic couplers

Calling a BBS. A BBS was literally just a computer that was at the other end of a phone line. To connect to that computer you would have to dial the phone number using a regular phone. Before modems could do this automatically you had to use something called an *acoustic coupler*. The *acoustic coupler* would translate the digital data coming form the computer, to an analog sound signal that would then travel over the phone line and would be interpreted by an acustic coupler at the other end and then back to digital signals. It was a way for computers to speak digital to each other using the old analog phone lines.

Here's a clip from the classic movie *War Games*, where you can see Matthew Broderick use an acoustic coupler.

<iframe width="560" height="315" src="https://www.youtube.com/embed/U2_h-EFlztY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Unlike today's subscriber lines that depend on a contract that goes back straight to your name and address. Acoustic couplers could be used on any phone, including payphones that you could find in the street. So as long as you could pay for the call, you didn't even need a phone line of your own.

<iframe width="560" height="315" src="https://www.youtube.com/embed/jYkeQkxkePY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Here's how the modulated signal of a modem would sound like in modem of different speeds as the technology evolved through the 70s and 80s. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/ckc6XSSh52w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Are BBS still alive?

There are still a few BBS out there, mostly run by nostalgic BBS lovers. These days however it is not necessary to call them up using a modem, you can access them through the internet using a program called `telnet`. Your mac used to have telnet installed by default, but Apple stopped including it in Mojave. You can install it again with `brew` though, like this:

```
brew install telnet
```

When you finally have telnet installed you can access the `Diamond Mine BBS` like this:

```
telnet bbs.dmine.net 24
```

Log in as `guest`.

- [A list of BBSes](https://www.telnetbbsguide.com/bbs/list/brief/)
- particlesbbs.dyndns.org:6400
- heatwave.ddns.net:9640
- blackflag.acid.org
- bbs.fozztexx.com
- cavebbs.homeip.net
- Historical list of BBS (North America only) [bbslist.textfiles.com](http://bbslist.textfiles.com/usbbs.html)

### MUDs

Tips to play in MUD:
- Keep paper by your side
- Most rooms have N/S/E/W directions, draw the rooms as you go along
- `look` is a commonly used command in most MUDs to get a description of the room

##### MOO

```
telnet lambda.moo.mud.org 8888
```

A [quick tutorial on MOO](https://www.cc.gatech.edu/classes/cs8113e_99_winter/lambda.html)

##### ZombieMUD

```
telnet zombiemud.org 3000
```

## Fall into the rabbithole 🐰
- [The Lost Civilization of Dial-Up Bulletin Board Systems](https://www.theatlantic.com/technology/archive/2016/11/the-lost-civilization-of-dial-up-bulletin-board-systems/506465/)
- [A RAPE IN CYBERSPACE: How an Evil Clown, a Haitian Trickster Spirit, Two Wizards, and a Cast of Dozens Turned a Database Into a Society](http://www.juliandibbell.com/texts/bungle_vv.html)
