---
title: The web-browser as a screen
---

Web-browsers have become increasingly versatile pieces of software, they are able to render a page written in the first spec of HTML in the 1990s, but they are also able to render the latest flashiest web application written with a dozen javascript frameworks. Browsers are hugely complex pieces of software, these days browsers are almost as complex as operating systems.

In this part of the course we will focus mostly on creating appealing visuals through code and sketching our ideas for animations and interactions, there are many ways of going about this and the default browser APIs are definitely sufficiently equipped to deal with most of what we can throw at it. However for this course we will be starting with p5js, there are several reasons for this. Those of you who studied design before might already be familiar with [Processing](http://www.processing.org), also Javascript is the *de facto* standard language for browser-based applications, even mobile apps and it is a good gateway language for other programming language of the same family as C# that we will be seeing later on in the year when we cover Unity.
