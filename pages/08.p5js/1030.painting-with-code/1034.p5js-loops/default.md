---
title: Shape repetitions using loops
---

Let's use a `for loop` to draw circles in different arrangements.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
}

function draw() {
    // paint it pink
    background(255, 0, 200);
    // do not draw a stroke around our shape
    noStroke();
    // change painting color to black
    fill(0);

    var xpos = 40;
    var ypos = height/2;
  
    for(let count = 0; count < 7; count++) {
      ellipse(xpos, ypos, 20, 20);
      xpos += 20;
    }
}
</script>

At the moment we are using the for loop to draw multiple circles and change their `x` coordinate. Let's try and change more than one parameter, let's change colors too.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    // create a working canvas
    createCanvas(200, 100);
}

function draw() {
    // paint it pink
    background(255, 0, 200);
    // do not draw a stroke around our shape
    noStroke();
    // change painting color to black
    fill(0);

    var xpos = 40;
    var ypos = height/2;
    var grey = 0;

    // repeat the block of code 7 times
    for(let count = 0; count < 7; count++) {
      fill(grey);
      ellipse(xpos, ypos, 20, 20);
      xpos += 20; // change the x position
      grey += 35; // change the tone of grey
    }
}
</script>

Let's use this same for loop but now instead of changing the position, let's change the radius of our circle, so that we can draw multiple concentric circles. This time we will also change the background color and the fill/stroke style of our circles.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    createCanvas(200, 200);
}

function draw() {
  background(0,0,0);
	noFill();
	stroke(255);
  rectMode(CENTER);

  let diam = 10;

  // repeat the block of code 7 times
  for(let count = 0; count < 7; count++) {
    rect(width/2, height/2, diam, diam);
    diam += 20; // increase diameter of our circle
  }
}
</script>

This looks neat, but I would like my circles to not be spaced equally as that makes my composition a little too boring. I would like the distance between the circles to get wider as the circles get bigger. For that I am going to increase the diameter each time, but this time I will make the increase a factor of the *golden ratio* number which is a constant with a value of `1.618033`.

<script type="text/p5" data-autoplay data-preview-width="250">
function setup() {
    createCanvas(200, 200);
}

function draw() {
  background(0,0,0);
	noFill();
	stroke(255);
  rectMode(CENTER);

  let diam = 10;

  // repeat the block of code 7 times
  for(let count = 0; count < 20; count++) {
    rect(width/2, height/2, diam, diam);
    diam += ( count * 1.618033 ); // increase diameter of our circle and multiple by golden ratio
  }
}
</script>

Using loops to create repetition in your visual patterns is an important aspect of algorithmic composition. There's literally a whole world to explore in drawing graphics using loops.

Another interesting technique to learn is using loops in combination with polar coordinates. This allows us to create compositions in which your graphical elements are laid out around circular patterns.
