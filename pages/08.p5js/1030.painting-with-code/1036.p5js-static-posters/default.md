---
title: Static poster compositions
---

Here you can find a couple of poster designs for a digital release, you can think of these as quick loops running in public screens. This design has quite a graphic look, with a grid layout composed of 6 rows and 3 columns. The middle row with the dynamic graphic elements is the only thing that changes, both designs are made using p5js primitives, namely `arc()` in the case of th first design and and `rect()` in the case of the second. What appears to the eye as a fancy animation is a `sin()` wave that modulates the sizes of our drawn elements.

[source for this poster](https://editor.p5js.org/zilog/sketches/lzi8r8Bwu)
<iframe  width="440" height="610" src="https://editor.p5js.org/zilog/embed/lzi8r8Bwu"></iframe>

[source for this poster](https://editor.p5js.org/zilog/sketches/HbDutD150)
<iframe  width="440" height="610" src="https://editor.p5js.org/zilog/embed/HbDutD150"></iframe>
<iframe width="440" height="610" src="https://editor.p5js.org/zilog/embed/Fk8VIoboJ"></iframe>

[source for this book cover](https://editor.p5js.org/zilog/sketches/Fk8VIoboJ)
