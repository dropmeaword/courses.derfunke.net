---
title: Understanding animation
---

Animation is a time-based medium, in and of itself it is a huge topic. You can spend years working in animation and learn something new every day. My hope with this session is to get you interested in it with a basic introduction and specifically see in which ways animation can be used in UI design.

Animation in the context of screen-based interactions can help us orchestrate an experience in various ways. Animation can take hold of a person's attention and be used to highlight a specific event. It can also give character to an experience, making it feeling unique. Animations can make something feel _well polished_ but if used in excess it can also make a product feel _overwhelming_. As everything in design, a good balance is what you should be aiming for. 

Simple animations can sometimes have profound effects, think for example of the LED animation in the original macbook pro, the 2013 edition. It really made it look like your laptop was breathing slowly as if it was asleep. Animation can also be used to guide us through a complex multi-step process. Animation be used to track time (this is used in its most prosaic form in spinners for example). Animation can also give attributes to our visuals that static pixels couldn't possibly convey. One way of conveying weight or lightness in the screen medium is to do it through animation. You can also convey emotion through animation like sadness, happines or anger.

As a designer you are primarily a visual communicator, you do not always have the luxury of words to convey meaning in a design. Animation is a visual language that will add depth of meaning to your work, and the way to use it wisely is to use it sparingly. Too much animation can ruin an experience by making it visually overwhelming, distracting and difficult to find a bearing, but a subtle animation here and there can add polish and make the experience feel memorable.

When looking at the versatility of animation in how we perceive screen-based experiences it helps to look at the masters. It helps to look at cartoons, specially old cartoons. You may consider this an assignment if you like... watch cartoons. 